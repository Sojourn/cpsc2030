//Boris Piha
//CPSC 2030
//SID 100199717
//Septermber - 16- 19
//reference used: https://www.w3schools.com

// constructors for the input data
function dataholder(){
    this.head =null;
    this.tail =null;
}

function dataUnit(nIn, r1In,r2In, i1In, i2In, xIn, yIn) {
  this.N = nIn;
  this.r1 = r1In;
  this.r2 = r2In;
  this.r;
  this.i1 = i1In;
  this.i2 = i2In;
  this.xinput = xIn;
  this.yinput = yIn;
  this.pString;
  this.back;
  this.forward;
  
}
//global object that holds the data
var bigData = new dataholder();
var i = 0;
var N;
var M = 4;
var r = 0;
var r1;
var r2;
var iout;
var itter;
var xin;
var yin;
var picstring;
var bigI = 0;

//function that adds to the data storage bigData
function build(){
    N = $("#N").val();
    r1 =$("#r1").val();
    r2 = $("#r2").val();
    iout = parseInt($("#i1").val());
    itter = eval($("#r2").val());
    xin = $("#x").val();
    yin = $("#y").val();
    if(i < $("#N").val()*(M)){
        if(bigData.head == null){
            bigData.head = new dataUnit($("#N").val(),$("#r1").val(),$("#r2").val(),($("#i1").val() + i),$("#i2").val(),$("#x").val(),$("#y").val());   
            bigData.tail = bigData.head;
            bigData.head.back = null;
            
            if(i % M < 2){
                bigData.tail.r = bigData.tail.r1;
            }
            else if(i % M >= 2){
                bigData.tail.r = bigData.tail.r2;
            }
            bigData.tail.xinput =  parseFloat(bigData.tail.r)*Math.cos(parseFloat(bigData.tail.i1)*2*Math.PI / (parseFloat(bigData.tail.N)*(M))) + parseFloat(bigData.tail.xinput);
            bigData.tail.yinput =  parseFloat(bigData.tail.r)*Math.sin(parseFloat(bigData.tail.i1)*2*Math.PI / (parseFloat(bigData.tail.N)*(M))) + parseFloat(bigData.tail.yinput);
            bigData.tail.pString = bigData.tail.xinput + "," + bigData.tail.yinput + " ";
            $( "#traceTable" ).append( "<tr><td>"+ i +"<td><td>"+ bigData.tail.r +"<td><td>"+bigData.tail.xinput +"<td><td>"+bigData.tail.yinput +"<td><td>"+bigData.tail.pString+"<td></tr>" );

            i++;
        }
        else{
            bigData.tail.forward = new dataUnit($("#N").val(),$("#r1").val(),$("#r2").val(),($("#i1").val() + i),$("#i2").val(),$("#x").val(),$("#y").val()); 
            bigData.tail.forward.back = bigData.tail;
            bigData.tail = bigData.tail.forward;
            if(i % M < 2){
                bigData.tail.r = bigData.tail.r1;
            }
            else if(i % M >= 2){
                bigData.tail.r = bigData.tail.r2;
            }
            
            bigData.tail.xinput =  parseFloat(bigData.tail.r)*Math.cos(parseFloat(bigData.tail.i1)*2*Math.PI / (parseFloat(bigData.tail.N)*(M))) + parseFloat(bigData.tail.xinput);
            bigData.tail.yinput =  parseFloat(bigData.tail.r)*Math.sin(parseFloat(bigData.tail.i1)*2*Math.PI / (parseFloat(bigData.tail.N)*(M))) + parseFloat(bigData.tail.yinput);
            bigData.tail.pString = bigData.tail.back.pString + bigData.tail.xinput + "," + bigData.tail.yinput + " ";
            $( "#traceTable" ).append( "<tr><td>"+ i +"<td><td>"+ bigData.tail.r +"<td><td>"+bigData.tail.xinput +"<td><td>"+bigData.tail.yinput +"<td><td>"+bigData.tail.pString+"<td></tr>" );
            i++;
        
        }
        svgBuilder(1);
    }
}
// function that removes a data unit
function destroy(){
    if(!(bigData.tail.back == null)){
        bigData.tail = bigData.tail.back;
        $("#traceTable tr:last-child").remove();
        i--;
        console.log("3");
        
    }
    else if(bigData.tail.back == null){
            bigData.head = null;
            bigData.tail = null;
            $("#traceTable tr:last-child").remove();
            i =0;
            console.log("4");
        }
    svgBuilder(1);
}
// function that resets big Data
function reset(){
    bigData.head =null;
    bigData.tail =null;
    for(let j =0; j < i; j++){
        $("#traceTable tr:last-child").remove();
    }
    i = 0;
    svgBuilder(2);
}

// function to build the svg element
function svgBuilder(x){
    if(x == 1){
        $("#output").attr("points", bigData.tail.pString);
    }
    else{
        $("#output").attr("points", "0,0");
    }
}
