
//global veriables
var hiddenOne = true;
var hiddenTwo = true;
var hiddenThree = true;

// hides and shows on click
function hideShowFunction(x){
    let stringIn = "#hiddenDiv" + x;
    if(x == "One"){
        if(hiddenOne){
            $(stringIn).fadeIn(1000);
            hiddenOne = false;
        }
        else{
            $(stringIn).fadeOut(1000);
            hiddenOne = true;
        }
    }
    if(x == "two"){
        if(hiddenTwo){
            $(stringIn).fadeIn(1000);
            hiddenTwo = false;
        }
        else{
            $(stringIn).fadeOut(1000);
            hiddenTwo = true;
        }
    }
    if(x == "three"){
        if(hiddenThree){
            $(stringIn).fadeIn(1000);
            hiddenThree = false;
        }
        else{
            $(stringIn).fadeOut(1000);
            hiddenThree = true;
        }
    }
    
}
// shows data on image click
function showDataImg(x, y){
    if(y == 1){
        $( x ).parent().children("div").show();
        $( x ).attr("onclick","showDataImg(this, 2)");
    }
    else{
        $( x ).parent().children("div").hide();
        $( x ).attr("onclick","showDataImg(this, 1)");
    }
}

// adds auctions to "your" auctions
function addToAuctions(x){
    $("#hiddenDivtwo").append("<div onclick='showDataImg(this, 1)'><img src=" + $(x).parent().parent().children('img').attr('src') +" onclick='showDataImg(this, 1)' alt='[Mec]' ><div hidden class='mecProfileDiv'><h3>Product Information</h3><p>Discription of mec goes here</p><button class='buttonStyle' type='button' onclick='removeFromBids(this)'>Remove!</button> </div></div>      ");
}
// removes auction from "your" auctions
function removeFromBids(x){
    $(x).parent().parent().remove();
}
// removes your mec from the auctions
function removeFromAuctions(x){
    let prductNum = "." + $(x).parent().parent().attr('class');
    $("#hiddenDivOne").children(prductNum).remove();
    $(x).parent().parent().remove();
}

