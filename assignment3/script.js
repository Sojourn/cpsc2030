//Boris Piha
//CPSC 2030
//SID 100199717
//Oct - 01
//reference used: fetch/promise examples and slides

//global variable used for navigating the fetched API
var offset = 0;

//calls the pokemon Api and updates the navigation bar
function callPokeApi(y){
    if(y == 1 && offset < 800){
        offset+= 20;
    }
    if(y == 2 && offset > 0){
        offset= offset - 20;
    }
    if(y == 1 || y == 2){
        for(let i = 0; i < 22; i++){
            $("#pokeList :last-child").remove();
        }
    }
    let x = "https://pokeapi.co/api/v2/pokemon-species?offset="+offset+"&limit=20";
    fetch(x).then(function (response) {
        console.log("tester2");
        if (!response.ok) {
            return Promise.reject("Network error");
        }
        return response.json();
     }).then(function (value) {
        console.log("tester33");
        if (value.length == 0) {
            return Promise.reject("No pokemon found")
        } else {
            for(let i = 0; i < 20; i++){
        
                console.log(value.next);
                $( "#pokeList" ).append( "<li onclick=run("+i+")>" + value.results[i].name + "</li>" );
            }
            $( "#pokeList" ).append( "<input type='submit' value='Back&nbsp&nbsp&nbsp&nbsp&nbsp ' onclick='callPokeApi("+"2"+")'></input>" );
            $( "#pokeList" ).append( "<input type='submit' value='Forward' onclick='callPokeApi("+"1"+")'></input>" );
            
            
            return value;
        }
  
     }).catch(function (rejection) {
        console.log(rejection);
     })
}

// calls the pokemon api from the nav bar and displays information i nthe main window
function run(x){
    let number = x + offset + 1;
    linkStr = "https://pokeapi.co/api/v2/pokemon-species/"+number+"/";

    fetch(linkStr).then(function (response) {
        if (!response.ok) {
            return Promise.reject("Network error");
        }
        return response.json();
        }).then(function (value) {

        if (value.length == 0) {
            return Promise.reject("No pokemon found")
        } else {
            $("#pokeName").text(value.name);
            for(let i = 0; i < 10; i++)
                if(value.flavor_text_entries[i].language.name == "en"){
                    $("#pokeInfo").text(value.flavor_text_entries[i].flavor_text);
                
                    i = 10;
                }
            return value;
        }
      
         }).catch(function (rejection) {
            console.log(rejection);
         })


}

// loads the navbar for the 1st time on page load, contains unused code 
function kicker(){
    //console.log("kickertest");
    //let linkStr = "https://pokeapi.co/api/v2/pokemon-species";
    callPokeApi(0);
    //console.log("kickertest2");
    //for(let i = 0; i < 20; i++){
    //    
    //    console.log(pokeObj.next);
    //    $( "#pokeList" ).append( pokeObj.results[i].name );
    //}
}